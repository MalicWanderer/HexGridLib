﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class HexGridPrototype : MonoBehaviour
{
    public float hexRadius = 1;
    public bool pointyTop = true;

    public int gridWidth = 10;
    public int gridHeight = 10;

    public int tilesX = 1;
    public int tilesY = 1;

    private Vector2[] cornerCoords = new Vector2[6];
    private Vector2[] uvCorners = new Vector2[6];

    // Use this for initialization
    void Start()
    {
        //calculate corner coordinates
        float turnOffset = pointyTop ? .5f : 0;

        for (int i = 0; i < 6; i++)
        {
            float inner = 2 * Mathf.PI * (i + turnOffset) / 6;

            var x = Mathf.Cos(inner);
            var y = Mathf.Sin(inner);

            uvCorners[i] = new Vector2((x + 1) / 2 / tilesX, (y + 1) / 2 / tilesY); //convert from -1 to 1 into 0 to 1
            cornerCoords[i] = new Vector2(hexRadius * x, hexRadius * y );

            Debug.Log(uvCorners[i].ToString("G4"));
        }

        //distances
        float diamater = hexRadius * 2;

        float vertDist = pointyTop ? diamater * 3 / 4 : Mathf.Sqrt(3f) / 2 * diamater;
        float horDist = pointyTop ? Mathf.Sqrt(3f) / 2 * diamater : diamater * 3 / 4;

        //verts and uvs, easier to do at same time
        Vector3[] verts = new Vector3[gridWidth * gridHeight * 6];
        Vector2[] uvs = new Vector2[gridHeight * gridWidth * 6];
        int v = 0;
        int u = 0;
        var tiles = tileOffsets().ToList();
        for (int x = 0; x < gridWidth; x++)
        {
            for (int y = 0; y < gridHeight; y++)
            {
                float xOffset = pointyTop ? ((y & 1) == 1 ? horDist / 2 : 0) : 0;
                float yOffset = !pointyTop ? ((x & 1) == 1 ? vertDist / 2 : 0) : 0;

                Vector2 center = new Vector2(x * horDist + xOffset, y * vertDist + yOffset);

                for (int i = 0; i < 6; i++)
                {
                    verts[v] = center + cornerCoords[i];
                    uvs[v] = uvCorners[i] + tiles[u];
                    v++;

                    
                }

                u++;
                if (u >= tilesX * tilesY)
                {
                    u = 0;
                }
            }
        }

        //tris
        int[] tris = new int[gridWidth * gridHeight * 3 * 4];
        var t = 0;
        for (int x = 0; x < gridWidth; x++)
        {
            for (int y = 0; y < gridHeight; y++)
            {

                int corner0 = (y * gridWidth + x) * 6;

                tris[t] = corner0 + 1;
                tris[t + 1] = corner0;
                tris[t + 2] = corner0 + 5;

                tris[t + 3] = corner0 + 2;
                tris[t + 4] = corner0 + 1;
                tris[t + 5] = corner0 + 5;

                tris[t + 6] = corner0 + 2;
                tris[t + 7] = corner0 + 5;
                tris[t + 8] = corner0 + 4;

                tris[t + 9] = corner0 + 2;
                tris[t + 10] = corner0 + 4;
                tris[t + 11] = corner0 + 3;

                t += 12;
            }
        }

        // mesh

        Mesh m = new Mesh();
        m.vertices = verts;
        m.triangles = tris;
        m.uv = uvs;

        GetComponent<MeshFilter>().mesh = m;
    }


    public IEnumerable<Vector2> tileOffsets()
    {
        for (int x = 0; x < tilesX; x++)
        {
            for (int y = 0; y < tilesY; y++)
            {
                yield return new Vector2((float)x / tilesX, (float)y / tilesY);
            }
        }
    }
}
