﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace HexGrid
{

    public static class BorderDrawer
    {
        public static void DrawBorders(LineRenderer linePrefab, Transform parent, IEnumerable<Vector3[]> points)
        {
            foreach (var list in points)
            {
                var line = Object.Instantiate(linePrefab);
                line.transform.SetParent(parent, false);

                //line.SetVertexCount(list.Length); //For earlier versions of unity.
                line.numPositions = list.Length;
                line.SetPositions(list);
            }
        }

        public static void DrawBorders(LineRenderer linePrefab, Transform parent, params Vector3[][] points)
        {
            DrawBorders(linePrefab, parent, points.AsEnumerable());
        }

        public static List<Dictionary<Hex, BitArray>> FloodFillBorders(Layout layout, Hex start, System.Func<Hex, bool> inside)
        {
            if (!inside(start)) throw new System.ArgumentException("Start must be inside.");

            var allBorders = new Dictionary<Border, Border>(new BorderDictComparer());
            var borderSquares = new Dictionary<Hex, List<Border>>();

            var q = new Queue<Hex>();
            var seen = new HashSet<Hex>();
            q.Enqueue(start);
            seen.Add(start);

            while (q.Count > 0)
            {
                var s = q.Dequeue();

                var ns = Hex.Neighbors(s).ToArray();
                for (int i = 0; i < ns.Length; i++)
                {
                    if (inside(ns[i]))
                    {
                        if (!seen.Contains(ns[i]))
                        {
                            seen.Add(ns[i]);
                            q.Enqueue(ns[i]);
                        }
                    }
                    else
                    {
                        var border = new Border(s, ns[i], layout);
                        allBorders.Add(border, border);
                        List<Border> list;
                        if (!borderSquares.TryGetValue(s, out list))
                        {
                            list = borderSquares[s] = new List<Border>();
                        }
                        list.Add(border);
                    }
                }
            }

            var borderQ = new Queue<Border>(allBorders.Keys);
            var seenBorders = new HashSet<Border>();
            var dirs = Enumerable.Range(0, 6).Select(i => (PointyDirection)i).ToArray();

            var res = new List<Dictionary<Hex, BitArray>>();

            while (borderQ.Count > 0)
            {
                var b = borderQ.Dequeue();
                if (seenBorders.Contains(b)) continue;

                seenBorders.Add(b);

                var cur = new Dictionary<Hex, BitArray>();

                var q2 = new Queue<Border>();
                q2.Enqueue(b);

                var hitBorders = new HashSet<Border>(allBorders.Comparer);

                while (q2.Count > 0)
                {
                    var b2 = q2.Dequeue();

                    Hex s;
                    PointyDirection dir;
                    var realB = allBorders[b2];
                    if (borderSquares.ContainsKey(realB.a))
                    {
                        s = realB.a;
                        dir = GetDirection(realB.b - realB.a);
                    }
                    else if (borderSquares.ContainsKey(realB.b))
                    {
                        s = realB.b;
                        dir = GetDirection(realB.a - realB.b);
                    }
                    else throw new System.ApplicationException("Somethings gone horribly wrong.");
                    var dirI = System.Array.IndexOf(dirs, dir);

                    BitArray bitArray;
                    if (!cur.TryGetValue(s, out bitArray))
                    {
                        bitArray = cur[s] = new BitArray(8, false);
                    }
                    bitArray[dirI] = true;

                    hitBorders.Add(b2);

                    foreach (var n in realB.Neighbors)
                    {
                        if (hitBorders.Contains(n)) continue;

                        if (allBorders.ContainsKey(n))
                        {
                            hitBorders.Add(n);
                            seenBorders.Add(n);
                            q2.Enqueue(n);
                        }
                    }
                }
                
                res.Add(cur);
            }

            return res;
        }

        static PointyDirection GetDirection(Hex hex)
        {
            for (int i = 0; i < 6; i++)
            {
                if (hex == Hex.Direction(i))
                {
                    return (PointyDirection)i;
                }
            }
            throw new System.ArgumentException(string.Format("{0} is not a direction.", hex));
        }

        class BorderDictComparer : IEqualityComparer<Border>
        {
            public bool Equals(Border x, Border y)
            {
                if (x.a == y.a && x.b == y.b) return true;
                if (x.b == y.a && x.a == y.b) return true;
                return false;
            }

            public int GetHashCode(Border obj)
            {
                return obj.a.GetHashCode() ^ obj.b.GetHashCode();
            }
        }

        public static List<Vector3> BorderPoints(Dictionary<Hex, BitArray> borders, Layout layout)
        {
            var points = new List<Vector3>();
            var pointSet = new HashSet<Vector3>();

            var dirs = Enumerable.Range(0, 6).Select(i => (PointyDirection)i).ToArray();
            Hex cur = borders.Keys.First();
            var last = new Dictionary<Hex, int>();
            var first = new Dictionary<Hex, int>();
            Hex? prev = null;

            var dir = borders.First().Value.OfType<bool>().Select((b, i) => new { b, i }).First(b => b.b).i;

            while (true)
            {
                if (prev != null)
                {
                    var n = Hex.Neighbor(cur, dirs[dir]);
                    var pr = prev.Value;
                    var prevDirN = n - pr;
                    var prevDir = System.Array.IndexOf(dirs, GetDirection(prevDirN));

                    var hit = false;
                    var done = false;
                    var i = first[pr];
                    while (!done)
                    {
                        if (i == prevDir)
                        {
                            hit = true;
                            break;
                        }
                        if (i == last[pr]) done = true;
                        i = (i + 1) % dirs.Length;
                    }
                    if (!hit)
                    {
                        var prevN = pr + Hex.Direction(dirs[first[pr]]);
                        var newDirN = prevN - cur;
                        dir = System.Array.IndexOf(dirs, GetDirection(newDirN));
                    }
                }

                if (!first.ContainsKey(cur))
                {
                    first[cur] = dir;
                }
                else
                {
                    if (first[cur] == dir) goto done;
                }

                //Debug.Log(cur);
                while (borders[cur][dir])
                {
                    var p = GetBorderPoint(cur, dirs[dir], layout);
                    if (!pointSet.Contains(p))
                    {
                        points.Add(p);
                        pointSet.Add(p);
                    }

                    dir = (dir + 1) % dirs.Length;

                    if (first[cur] == dir) goto done;
                }

                last[cur] = dir;
                prev = cur;
                cur = cur + Hex.Direction(dirs[dir]);
                dir = Opposite(dir + 1);
            }

            done:
            points.Add(points[0]);
            //points.Add(points[1]);

            return points;
        }

        static int Opposite(int dir)
        {
            dir += 3;
            return dir % 6;
        }

        static readonly float sizeToInnerSize = .5f * Mathf.Sqrt(3);
        static Vector3 GetBorderPoint(Hex hex, PointyDirection dir, Layout layout)
        {
            return layout.HexToWorld(hex) + layout.HexToWorld(Hex.Direction(dir)).normalized * (layout.size * sizeToInnerSize);
        }

        [System.Diagnostics.DebuggerDisplay("{a} - {b}")]
        private struct Border
        {
            public readonly Hex a, b;
            public readonly Layout layout;
            public Border(Hex a, Hex b, Layout layout)
            {
                if (Hex.Distance(a, b) != 1) throw new System.ArgumentException("Border coords must be neighbors.");
                this.a = a;
                this.b = b;
                this.layout = layout;
            }

            public Vector3 Point
            {
                get
                {
                    var a = layout.HexToWorld(this.a);
                    var b = layout.HexToWorld(this.b);
                    var d = a - b;
                    return (b + d * .5f);
                }
            }

            public Vector2 XYPoint
            {
                get
                {
                    var a = layout.HexToWorld2D(this.a);
                    var b = layout.HexToWorld2D(this.b);
                    var d = a - b;
                    return b + d * .5f;
                }
            }

            public Vector3 XZPoint
            {
                get
                {
                    var a = layout.HexToWorld3D(this.a);
                    var b = layout.HexToWorld3D(this.b);
                    var d = a - b;
                    return b + d * .5f;
                }
            }

            public override bool Equals(object obj)
            {
                if (!(obj is Border)) return false;
                var other = (Border)obj;
                return other.Point == Point;
            }

            public override int GetHashCode()
            {
                return Point.GetHashCode();
            }

            public bool IsNieghbor(Border other)
            {
                if (a == other.a)
                {
                    return (Hex.Distance(b, other.b) == 1);
                }
                if (b == other.b)
                {
                    return (Hex.Distance(a, other.a) == 1);
                }
                if (a == other.b)
                {
                    return Hex.Distance(b, other.a) == 1;
                }
                if (b == other.a)
                {
                    return Hex.Distance(a, other.b) == 1;
                }

                return false;
            }

            public IEnumerable<Border> Neighbors
            {
                get
                {
                    var a = this.a;
                    var b = this.b;
                    var layout = this.layout;
                    return Enumerable.Empty<Border>()
                        .Concat(Hex.Neighbors(a).Select(h => new Border(h, a, layout)))
                        .Concat(Hex.Neighbors(b).Select(h => new Border(h, b, layout)))
                        .Where(IsNieghbor);
                }
            }
        }

        public static IEnumerable<Vector3> BuffCorners(IEnumerable<Vector3> list)
        {
            var enumerator = list.GetEnumerator();
            if (!enumerator.MoveNext())
            {
                yield break;
            }

            var prev = enumerator.Current;
            yield return prev;

            if (!enumerator.MoveNext())
            {
                yield break;
            }

            var prevPrev = prev;
            prev = enumerator.Current;
            var prevDir = prev - prevPrev;

            while (enumerator.MoveNext())
            {
                var cur = enumerator.Current;
                var dir = cur - prev;

                var angle = Vector3.Angle(prevDir, dir);
                if (angle > 30)
                {
                    yield return prevPrev + prevDir * .9f;
                }

                yield return prev;
                prevPrev = prev;
                prev = cur;
                prevDir = dir;
            }

            yield return prev;
        }
    }
}