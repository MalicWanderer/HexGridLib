﻿using UnityEngine;
using System.Collections;
using HexGrid;
using System.Linq;

public class BorderExample : HexGridRectangle
{
    public LineRenderer linePrefab;

    public HexArea[] areas, holes;

    // Use this for initialization
    protected override void Start()
    {
        base.Start();

        if (linePrefab == null)
        {
            Debug.LogError("Line prefab not assigned.");
            return;
        }

        if (areas == null || areas.Length < 1)
        {
            Debug.LogError("No border areas set.");
            return;
        }

        var fillResults = BorderDrawer.FloodFillBorders(layout, areas[0].center, h => areas.Any(a => a.Contains(h)) && !holes.Any(a => a.Contains(h)));
        var points = fillResults.Select(d => BorderDrawer.BorderPoints(d, layout));
        BorderDrawer.DrawBorders(linePrefab, transform, points.Select(p => p.ToArray()));
    }
}

[System.Serializable]
public struct HexArea
{
    public Hex center;
    public int radius;

    public bool Contains(Hex h)
    {
        return Hex.Distance(h, center) < radius;
    }
}