﻿namespace HexGrid
{
    using UnityEngine;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Basic type for storing Hex coordinates.
    /// </summary>
    [System.Serializable]
    public struct Hex
    {
        /// <summary>
        /// Field for storing coordinates, do not modify unless you know what you're doing.
        /// </summary>
        public int q, r, s;

        /// <summary>
        /// Construct Hex from three coordinate values. The three values must add up to 0.
        /// </summary>
        public Hex(int q, int r, int s)
        {
            Debug.Assert(q + r + s == 0);

            this.q = q;
            this.r = r;
            this.s = s;
        }

        /// <summary>
        /// Construct Hex from two coordinate values, automatically calculating the third.
        /// </summary>
        public Hex(int q, int r)
        {
            this.q = q;
            this.r = r;
            s = -q - r;
        }

        /// <summary>
        /// After setting one or two coordinate values, call this to auto-calculate the third.
        /// </summary>
        /// <param name="axis1">The first set Axis to base the third on.</param>
        /// <param name="axis2">The second set Axis to base the third on.</param>
        public void SetThirdAxis(ShapeGenerators.Axis axis1, ShapeGenerators.Axis axis2)
        {
            switch (axis1)
            {
                case ShapeGenerators.Axis.q:
                    switch (axis2)
                    {
                        case ShapeGenerators.Axis.q:
                            throw new System.ArgumentException("Both axis cannot be the same");
                        case ShapeGenerators.Axis.r:
                            s = -q - r;
                            return;
                        case ShapeGenerators.Axis.s:
                            r = -q - s;
                            return;
                        default:
                            throw new System.ArgumentException("Invalid axis.");
                    }
                case ShapeGenerators.Axis.r:
                    switch (axis2)
                    {
                        case ShapeGenerators.Axis.q:
                            s = -q - r;
                            return;
                        case ShapeGenerators.Axis.r:
                            throw new System.ArgumentException("Both axis cannot be the same");
                        case ShapeGenerators.Axis.s:
                            q = -r - s;
                            return;
                        default:
                            throw new System.ArgumentException("Invalid axis.");
                    }
                case ShapeGenerators.Axis.s:
                    switch (axis2)
                    {
                        case ShapeGenerators.Axis.q:
                            r = -q - s;
                            return;
                        case ShapeGenerators.Axis.r:
                            q = -r - s;
                            return;
                        case ShapeGenerators.Axis.s:
                            throw new System.ArgumentException("Both axis cannot be the same");
                        default:
                            throw new System.ArgumentException("Invalid axis.");
                    }
                default:
                    throw new System.ArgumentException("Invalid axis.");
            }
        }

        /// <summary>
        /// Access coordinate values (q, r, and s) by index (0, 1, and 2).
        /// </summary>
        public int this[int index]
        {
            get
            {
                switch (index)
                {
                    case 0:
                        return q;
                    case 1:
                        return r;
                    case 2:
                        return s;
                    default:
                        throw new System.IndexOutOfRangeException("Hex coord index must be 0, 1, or 2.");
                }
            }
            set
            {
                switch (index)
                {
                    case 0:
                        q = value;
                        break;
                    case 1:
                        r = value;
                        break;
                    case 2:
                        s = value;
                        break;
                    default:
                        throw new System.IndexOutOfRangeException("Hex coord index must be 0, 1, or 2.");
                }
            }
        }

        public static bool operator ==(Hex a, Hex b)
        {
            return a.q == b.q && a.r == b.r && a.s == b.s;
        }

        public static bool operator !=(Hex a, Hex b)
        {
            return !(a == b);
        }

        public static Hex operator +(Hex a, Hex b)
        {
            return new Hex(a.q + b.q, a.r + b.r, a.s + b.s);
        }

        public static Hex operator -(Hex a, Hex b)
        {
            return new Hex(a.q - b.q, a.r - b.r, a.s - b.s);
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("Hex ({0}, {1}, {2})", q, r, s);
        }

        /// <summary>
        /// Get distance from the given Hex to the Origin (0, 0, 0) Hex.
        /// </summary>
        /// <param name="hex">Hex to measure the length of.</param>
        /// <returns></returns>
        public static int Length(Hex hex)
        {
            return (int)((Mathf.Abs(hex.q) + Mathf.Abs(hex.r) + Mathf.Abs(hex.s)) / 2);
        }

        /// <summary>
        /// Get distance between two Hexes.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int Distance(Hex a, Hex b)
        {
            return Length(a - b);
        }

        readonly static Hex[] directions = {new Hex(1, 0, -1), new Hex(1, -1, 0), new Hex(0, -1, 1),
                                        new Hex(-1, 0, 1), new Hex(-1, 1, 0), new Hex(0, 1, -1) };

        /// <summary>
        /// Get Hex one unit from the origin in the specified direction. Prefer using one of the other overloads for more readability.
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        public static Hex Direction(int direction)
        {
            //if (direction < 0 || direction > 5) throw new System.ArgumentOutOfRangeException("direction", "Direction must be between 0 and 5.");
            direction = direction % 6;
            if (direction < 0) direction = direction + 6;
            return directions[direction];
        }

        /// <summary>
        /// Get Hex one unit from the origin in the specified direction.
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        public static Hex Direction(FlatDirection dir)
        {
            return Direction((int)dir);
        }

        /// <summary>
        /// Get Hex one unit from the origin in the specified direction.
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        public static Hex Direction(PointyDirection dir)
        {
            return Direction((int)dir);
        }

        /// <summary>
        /// Get the Hex one unit from the given Hex in the specified direction. Prefer using one of the other overloads for more readability.
        /// </summary>
        /// <param name="hex"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public static Hex Neighbor(Hex hex, int direction)
        {
            return hex + Direction(direction);
        }

        /// <summary>
        /// Get the Hex one unit from the given Hex in the specified direction.
        /// </summary>
        /// <param name="hex"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public static Hex Neighbor(Hex hex, FlatDirection dir)
        {
            return Neighbor(hex, (int)dir);
        }

        /// <summary>
        /// Get the Hex one unit from the given Hex in the specified direction.
        /// </summary>
        /// <param name="hex"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public static Hex Neighbor(Hex hex, PointyDirection dir)
        {
            return Neighbor(hex, (int)dir);
        }

        /// <summary>
        /// Get all neighbors of the given Hex.
        /// </summary>
        /// <param name="hex"></param>
        /// <returns></returns>
        public static IEnumerable<Hex> Neighbors(Hex hex)
        {
            for (int i = 0; i < 6; i++)
            {
                yield return Neighbor(hex, i);
            }
        }

        /// <summary>
        /// Get the Hex closest to the given fractional point in Hex-space. Used mostly by Layout for converting world to Hex coordinates.
        /// </summary>
        /// <param name="fractionalHex"></param>
        /// <returns></returns>
        public static Hex Round(Vector3 fractionalHex)
        {
            int q = (int)(Mathf.Round(fractionalHex.x));
            int r = (int)(Mathf.Round(fractionalHex.y));
            int s = (int)(Mathf.Round(fractionalHex.z));
            float q_diff = Mathf.Abs(q - fractionalHex.x);
            float r_diff = Mathf.Abs(r - fractionalHex.y);
            float s_diff = Mathf.Abs(s - fractionalHex.z);
            if (q_diff > r_diff && q_diff > s_diff)
            {
                q = -r - s;
            }
            else if (r_diff > s_diff)
            {
                r = -q - s;
            }
            else
            {
                s = -q - r;
            }
            return new Hex(q, r, s);
        }

        static Vector3 hex_lerp(Hex a, Hex b, float t)
        {
            return new Vector3(a.q + (b.q - a.q) * t,
                                 a.r + (b.r - a.r) * t,
                                 a.s + (b.s - a.s) * t);
        }

        /// <summary>
        /// Get all Hexes on a line from one Hex to another.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static IEnumerable<Hex> DrawLine(Hex a, Hex b)
        {
            int N = Distance(a, b);
            float step = 1f / Mathf.Max(N, 1);

            for (int i = 0; i <= N; i++)
            {
                yield return Round(hex_lerp(a, b, step * i));
            }
        }

        /// <summary>
        /// Get the Hex corresponding to the given Hex rotated around the origin Hex the specified multiple of 60 degrees.
        /// </summary>
        /// <param name="hex"></param>
        /// <param name="rotation">The multiple of 60 degrees to rotate the given hex around the origin.</param>
        /// <returns></returns>
        public static Hex Rotate(Hex hex, int rotation)
        {
            if (rotation < 0) rotation = rotation % 6 + 6; //convert negative rotation to positive.

            var mult = (rotation & 1) == 1 ? -1 : 1; //odd rotations flip the coordinates.

            return new Hex(hex[(0 + rotation) % 3] * mult,
                           hex[(1 + rotation) % 3] * mult,
                           hex[(2 + rotation) % 3] * mult);

        }

        /// <summary>
        /// Get the Hex corresponding to the given Hex rotated around the given center Hex the specified multiple of 60 degrees.
        /// </summary>
        /// <param name="hex">The Hex to rotate.</param>
        /// <param name="center">The Hex to rotate around.</param>
        /// <param name="rotation">The multiple of 60 degrees to rotate.</param>
        /// <returns></returns>
        public static Hex RotateAround(Hex hex, Hex center, int rotation)
        {
            return Rotate(hex - center, rotation) + center;
        }

        /// <summary>
        /// Get the Hex neighboring this hex in the specified direction. Prefer one of the other overloads for more readability.
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        public Hex Neighbor(int direction)
        {
            return Neighbor(this, direction);
        }

        /// <summary>
        /// Get the Hex neighboring this hex in the specified direction.
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        public Hex Neighbor(FlatDirection dir)
        {
            return Neighbor(this, dir);
        }

        /// <summary>
        /// Get the Hex neighboring this hex in the specified direction.
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        public Hex Neighbor(PointyDirection dir)
        {
            return Neighbor(this, dir);
        }

        /// <summary>
        /// Get all Hexes neighboring this hex.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Hex> Neighbors()
        {
            return Neighbors(this);
        }

        /// <summary>
        /// Get the Hex corresponding to this Hex rotated around the origin Hex the specified multiple of 60 degrees.
        /// </summary>
        /// <param name="rotation">The multiple of 60 degrees to rotate around the origin.</param>
        /// <returns></returns>
        public Hex Rotate(int rotation)
        {
            return Rotate(this, rotation);
        }

        /// <summary>
        /// Get the distance from this Hex to the origin Hex.
        /// </summary>
        /// <returns></returns>
        public int Length()
        {
            return Length(this);
        }

        /// <summary>
        /// Get the Hex corresponding to this Hex rotated around the given center Hex the specified multiple of 60 degrees.
        /// </summary>
        /// <param name="center">The Hex to rotate around.</param>
        /// <param name="rotation">The multiple of 60 degrees to rotate.</param>
        /// <returns></returns>
        public Hex RotateAround(Hex center, int rotation)
        {
            return RotateAround(this, center, rotation);
        }
    }

    /// <summary>
    /// Named direction values for Pointy-Topped Hexes.
    /// </summary>
    public enum PointyDirection
    {
        NorthEast = 5,
        East = 0,
        SouthEast = 1,
        SouthWest = 2,
        West = 3,
        NorthWest = 4,
    }

    /// <summary>
    /// Named direction values for Flat-Topped Hexes.
    /// </summary>
    public enum FlatDirection
    {
        NorthEast = 0,
        SouthEast = 1,
        South = 2,
        SouthWest = 3,
        NorthWest = 4,
        North = 5,
    }

    /// <summary>
    /// Class containing constant values used by the Layout class.
    /// </summary>
    public class Orientation
    {
        public readonly float f0, f1, f2, f3;
        public readonly float b0, b1, b2, b3;
        public readonly float startAngle;

        private Orientation(float f0_, float f1_, float f2_, float f3_,
                            float b0_, float b1_, float b2_, float b3_,
                            float startAngle_)
        {
            f0 = f0_;
            f1 = f1_;
            f2 = f2_;
            f3 = f3_;
            b0 = b0_;
            b1 = b1_;
            b2 = b2_;
            b3 = b3_;
            startAngle = startAngle_;
        }

        /// <summary>
        /// Orientation for Pointy-Topped Hexes.
        /// </summary>
        public static readonly Orientation pointy = new Orientation(Mathf.Sqrt(3), Mathf.Sqrt(3) / 2, 0, 3f / 2f,
                                                                    Mathf.Sqrt(3) / 3, -1 / 3f, 0, 2f / 3f,
                                                                    .5f);

        /// <summary>
        /// Orientation for Flat-Topped Hexes.
        /// </summary>
        public static readonly Orientation flat = new Orientation(3f / 2f, 0, Mathf.Sqrt(3) / 2, Mathf.Sqrt(3),
                                                                  2f / 3f, 0, -1f / 3f, Mathf.Sqrt(3) / 3f,
                                                                  0);
    }

    /// <summary>
    /// Class for handling conversions between Hex and World space.
    /// </summary>
    public class Layout
    {
        public readonly Orientation orientation;
        public readonly float size;
        public readonly Vector2 origin;
        public readonly bool default3d;
        public readonly float yOffset3d;

        private Vector2[] cornerCoords = new Vector2[6];

        /// <summary>
        /// Construct a Layout for converting between Hex and World space.
        /// </summary>
        /// <param name="orientation">The orientation of the hexes. (Pointy or Flat topped.)</param>
        /// <param name="size">The world space size of a single Hex, as a radius from center to any corner.</param>
        /// <param name="origin">World space location of the origin (0, 0, 0) Hex.</param>
        public Layout(Orientation orientation, float size, Vector3 origin, bool default3d = false)
        {
            this.orientation = orientation;
            this.size = size;
            this.origin = origin;
            this.default3d = default3d;

            if (default3d)
            {
                this.origin = origin.To2D();
                yOffset3d = origin.y;
            }

            for (int i = 0; i < 6; i++)
            {
                float angle = 2 * Mathf.PI * (i + orientation.startAngle) / 6;

                var x = Mathf.Cos(angle);
                var y = Mathf.Sin(angle);

                cornerCoords[i] = new Vector2(size * x, size * y);
            }
        }

        /// <summary>
        /// World space coordinates of the center of the given Hex.
        /// </summary>
        /// <param name="h"></param>
        /// <returns></returns>
        public Vector3 HexToWorld(Hex h)
        {
            return default3d ? HexToWorld3D(h) : (Vector3)HexToWorld2D(h);
        }

        /// <summary>
        /// Convert world space coordinates to hex space.
        /// </summary>
        /// <param name="p">World space point to convert to hex space.</param>
        /// <returns>A Vector3 representing the FractionalHex corresponding to the given world space coordinates.
        /// For a whole Hex, use WorldToRoundHex or pass this return value to Hex.Round.</returns>
        public Vector3 WorldToHex(Vector3 p)
        {
            return default3d ? WorldToHex3D(p) : WorldToHex2D(p);
        }

        /// <summary>
        /// Get the Specific Hex a given world space point is located in.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Hex WorldToRoundHex(Vector3 p)
        {
            return Hex.Round(WorldToHex(p));
        }

        /// <summary>
        /// Get the world space offset from the center of a Hex to one of it's corners.
        /// </summary>
        /// <param name="i">The corner # to get the offset to.</param>
        /// <returns></returns>
        public Vector3 CornerOffset(int i)
        {
            return default3d ? cornerCoords[i].To3D() : (Vector3)cornerCoords[i];
        }

        /// <summary>
        /// Get all the world space corners of the given Hex.
        /// </summary>
        /// <param name="h"></param>
        /// <returns></returns>
        public Vector3[] HexCorners(Hex h)
        {
            return default3d ? HexCorners3D(h) : HexCorners2D(h).Select(v => (Vector3)v).ToArray();
        }

        /// <summary>
        /// World space coordinates of the center of the given Hex, on the XY plane.
        /// </summary>
        /// <param name="h"></param>
        /// <returns></returns>
        public Vector2 HexToWorld2D(Hex h)
        {
            var M = orientation;

            float x = (M.f0 * h.q + M.f1 * h.r) * this.size;
            float y = (M.f2 * h.q + M.f3 * h.r) * this.size;
            return new Vector2(x + this.origin.x, y + this.origin.y);
        }

        /// <summary>
        /// Convert XY world space coordinates to hex space.
        /// </summary>
        /// <param name="p">World space point to convert to hex space.</param>
        /// <returns>A Vector3 representing the FractionalHex corresponding to the given world space coordinates.
        /// For a whole Hex, use WorldToRoundHex or pass this return value to Hex.Round.</returns>
        public Vector3 WorldToHex2D(Vector2 p)
        {
            var M = this.orientation;
            Vector2 pt = new Vector2((p.x - this.origin.x) / this.size,
                             (p.y - this.origin.y) / this.size);
            float q = M.b0 * pt.x + M.b1 * pt.y;
            float r = M.b2 * pt.x + M.b3 * pt.y;
            return new Vector3(q, r, -q - r);
        }

        /// <summary>
        /// Get the Specific Hex a given XY world space point is located in.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Hex WorldToRoundHex2D(Vector2 p)
        {
            return Hex.Round(WorldToHex2D(p));
        }

        /// <summary>
        /// Get the world space offset from the center of a Hex to one of it's corners, on the XY plane.
        /// </summary>
        /// <param name="i">The corner # to get the offset to.</param>
        /// <returns></returns>
        public Vector2 CornerOffset2D(int i)
        {
            return cornerCoords[i];
        }

        /// <summary>
        /// Get all the world space corners of the given Hex, along the XY plane.
        /// </summary>
        /// <param name="h"></param>
        /// <returns></returns>
        public Vector2[] HexCorners2D(Hex h)
        {
            var corners = new Vector2[6];
            var center = HexToWorld2D(h);

            for (int i = 0; i < cornerCoords.Length; i++)
            {
                corners[i] = cornerCoords[i] + center;
            }

            return corners;
        }

        /// <summary>
        /// World space coordinates of the center of the given Hex, on the XZ plane.
        /// </summary>
        /// <param name="h"></param>
        /// <returns></returns>
        public Vector3 HexToWorld3D(Hex h)
        {
            return HexToWorld2D(h).To3D(yOffset3d);
        }

        /// <summary>
        /// Convert XZ world space coordinates to hex space.
        /// </summary>
        /// <param name="p">World space point to convert to hex space.</param>
        /// <returns>A Vector3 representing the FractionalHex corresponding to the given world space coordinates.
        /// For a whole Hex, use WorldToRoundHex or pass this return value to Hex.Round.</returns>
        public Vector3 WorldToHex3D(Vector3 p)
        {
            return WorldToHex2D(p.To2D());
        }

        /// <summary>
        /// Get the Specific Hex a given XZ world space point is located in.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Hex WorldToRoundHex3D(Vector3 p)
        {
            return WorldToRoundHex2D(p.To2D());
        }

        /// <summary>
        /// Get the world space offset from the center of a Hex to one of it's corners, on the XZ plane.
        /// </summary>
        /// <param name="i">The corner # to get the offset to.</param>
        /// <returns></returns>
        public Vector3 CornerOffset3D(int i)
        {
            return cornerCoords[i].To3D();
        }

        /// <summary>
        /// Get all the world space corners of the given Hex, along the XZ plane.
        /// </summary>
        /// <param name="h"></param>
        /// <returns></returns>
        public Vector3[] HexCorners3D(Hex h)
        {
            var corners = new Vector3[6];
            var center = HexToWorld3D(h);

            for (int i = 0; i < cornerCoords.Length; i++)
            {
                corners[i] = cornerCoords[i].To3D() + center;
            }

            return corners;
        }
    }

    /// <summary>
    /// Container class for extension methods on Unity types.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Returns a Vector3 with this vector's x component, and this vectors y component as it's z component.
        /// Essentially moving the vector from the XY plane to the XZ plane.
        /// </summary>
        /// <param name="v">The vector to transform.</param>
        /// <returns>The input vector moved to the XZ plane.</returns>
        public static Vector3 To3D(this Vector2 v, float newY = 0)
        {
            return new Vector3(v.x, newY, v.y);
        }

        /// <summary>
        /// Returns a Vector3 with this vector's x component, and this vectors y component as it's z component.
        /// Essentially moving the vector from the XY plane to the XZ plane.
        /// </summary>
        /// <param name="v">The vector to transform.</param>
        /// <returns>The input vector moved to the XZ plane.</returns>
        public static Vector3 To3D(this Vector3 v, float newY = 0)
        {
            return new Vector3(v.x, newY, v.y);
        }

        /// <summary>
        /// Returns a Vector2 with this vector's x component, and this vectors z component as it's y component.
        /// Essentially moving the vector from the XZ plane to the XY plane.
        /// </summary>
        /// <param name="v">The vector to transform.</param>
        /// <returns>The input vector moved to the XY plane.</returns>
        public static Vector2 To2D(this Vector3 v)
        {
            return new Vector2(v.x, v.z);
        }
    }

    /// <summary>
    /// Class to handle generating Meshes from of collections of Hexes.
    /// </summary>
    public class Grid
    {

        public readonly Layout layout;
        public readonly int tilesX, tilesY;

        private Vector2[] uvCorners = new Vector2[6];

        /// <summary>
        /// Construct a Grid from the given Layout, with an optional number of X and Y texture tiles.
        /// </summary>
        /// <param name="layout">The Layout to use for converting from Hexes to world coordinates.</param>
        /// <param name="tilesX">The number of tiles in the X direction of the target texture.</param>
        /// <param name="tilesY">The number of tiles in the Y direction of the target texture.</param>
        public Grid(Layout layout, int tilesX = 1, int tilesY = 1)
        {
            this.layout = layout;
            this.tilesX = tilesX;
            this.tilesY = tilesY;

            for (int i = 0; i < 6; i++)
            {
                //remove the size of the hexagon from the corner coordinates (as if it was size 1)
                //then convert from -1 to 1 into 0 to 1, then divide by number tiles so it's one corner tile
                //can add offsets later for other tiles
                uvCorners[i] = new Vector2(((layout.CornerOffset2D(i).x / layout.size) + 1) / 2 / tilesX, ((layout.CornerOffset2D(i).y / layout.size) + 1) / 2 / tilesY);
            }
        }

        private IEnumerable<Vector2> tileOffsets()
        {
            //outer loop on y so that tiles 'read' left-right first, Then bottom-top
            for (int y = 0; y < tilesY; y++)
            {
                for (int x = 0; x < tilesX; x++)
                {
                    yield return new Vector2((float)x / tilesX, (float)y / tilesY);
                }
            }
        }

        /// <summary>
        /// Generate a mesh from the given list of hexes. All Hexes will use the first tile in the texture with all vertexes colored white.
        /// </summary>
        /// <param name="hexes"></param>
        /// <returns></returns>
        public Mesh MakeMesh(IEnumerable<Hex> hexes)
        {
            return MakeMesh(hexes, (h) => 0);
        }

        /// <summary>
        /// Generate a mesh from the given list of hexes, using the given selector to associate each Hex with a tile, and an optional selector for each hex's vertex colors.
        /// </summary>
        /// <param name="hexes">Collection of hexes to build the mesh from.</param>
        /// <param name="tileSelector">Function to select which tile each hex is associated with.
        /// Tiles are numbered starting from 0 at the bottom left, proceeding left to right then top to bottom.</param>
        /// <param name="colorSelector">Function to select the vertex color for each hex.</param>
        /// <returns></returns>
        public Mesh MakeMesh(IEnumerable<Hex> hexes, System.Func<Hex, int> tileSelector, System.Func<Hex, Color> colorSelector = null)
        {
            //verts and uvs, easier to do at same time
            var verts = new List<Vector3>();
            var uvs = new List<Vector2>();

            var col = new List<Color>();

            if (colorSelector == null)
            {
                colorSelector = (h) => Color.white;
            }

            int hexCount = 0;
            var tiles = tileOffsets().ToList();

            foreach (var h in hexes)
            {
                var corners = layout.HexCorners(h);

                for (int j = 0; j < corners.Length; j++)
                {
                    verts.Add(corners[j]);
                    uvs.Add(uvCorners[j] + tiles[tileSelector(h)]);
                    col.Add(colorSelector(h));
                }
                hexCount++;
            }

            //tris
            int[] tris = new int[hexCount * 3 * 4];
            var t = 0;

            for (int i = 0; i < hexCount; i++)
            {
                int corner0 = i * 6;

                tris[t] = corner0 + 1;
                tris[t + 1] = corner0;
                tris[t + 2] = corner0 + 5;

                tris[t + 3] = corner0 + 2;
                tris[t + 4] = corner0 + 1;
                tris[t + 5] = corner0 + 5;

                tris[t + 6] = corner0 + 2;
                tris[t + 7] = corner0 + 5;
                tris[t + 8] = corner0 + 4;

                tris[t + 9] = corner0 + 2;
                tris[t + 10] = corner0 + 4;
                tris[t + 11] = corner0 + 3;

                t += 12;
            }

            // mesh

            Mesh m = new Mesh();
            m.vertices = verts.ToArray();
            m.triangles = tris;
            m.uv = uvs.ToArray();
            m.colors = col.ToArray();
            m.RecalculateNormals();

            return m;
        }

        /// <summary>
        /// Generate a mesh from the given dictionary mapping hexes to tile numbers.
        /// </summary>
        /// <param name="map"></param>
        /// <returns></returns>
        public Mesh MakeMesh(IDictionary<Hex, int> map)
        {
            var hexList = map.Keys.ToList();

            return MakeMesh(hexList, (h) => map[h]);
        }
    }

    /// <summary>
    /// Basic type for representing Hexes in the Offset coordinate system, useful for rectangular hex grids/maps.
    /// </summary>
    [System.Serializable]
    public struct OffsetCoord
    {
        /// <summary>
        /// The Types of OffsetCoord (Pointy or Flat topped.)
        /// </summary>
        public enum Type
        {
            Pointy = 1,
            Flat = 0
        }

        /// <summary>
        /// Enumeration for whether an OffsetCoords are offset on Even or Odd Rows/Columns.
        /// </summary>
        public enum Which
        {
            Even = 1,
            Odd = -1,
        }

        /// <summary>
        /// Horizontal coordinate value. Roughly analogous to X in a square grid.
        /// </summary>
        public int col;

        /// <summary>
        /// Vertical coordinate value. Roughly analogous to Y in a square grid.
        /// </summary>
        public int row;

        /// <summary>
        /// Construct an OffsetCoord from the given horizontal and vertical coordinates.
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        public OffsetCoord(int col, int row)
        {
            this.col = col;
            this.row = row;
        }

        /// <summary>
        /// Construct an OffsetCoord from the given Hex, Pointy or Flat orientation, and Even or Odd offset.
        /// </summary>
        /// <param name="h">Hex to convert.</param>
        /// <param name="t">Pointy or Flat orientation.</param>
        /// <param name="w">Even or Odd offset.</param>
        public OffsetCoord(Hex h, Type t, Which w)
        {
            switch (t)
            {
                case Type.Pointy:
                    col = h.q + (h.r + (int)w * (h.r & 1)) / 2; //not sure why they cast here?
                    row = h.r;
                    return;
                case Type.Flat:
                    col = h.q;
                    row = h.r + (h.q + (int)w * (h.q & 1)) / 2;
                    return;
                default:
                    throw new System.ArgumentException("Invalid grid type.");
            }
        }

        /// <summary>
        /// Convert this OffsetCoord to a regular Hex value.
        /// </summary>
        /// <param name="t">Convert to a Pointy or Flat Hex.</param>
        /// <param name="w">Convert from an Even or Odd offset.</param>
        /// <returns></returns>
        public Hex ToHex(Type t, Which w)
        {
            switch (t)
            {
                case Type.Pointy:
                    int q = col - (row + (int)w * (row & 1)) / 2;
                    int r = row;
                    return new Hex(q, r);
                case Type.Flat:
                    q = col;
                    r = row - (col + (int)w * (col & 1)) / 2;
                    return new Hex(q, r);
                default:
                    throw new System.ArgumentException("Invalid grid type.");
            }
        }

        public override string ToString()
        {
            return string.Format("({0}, {1})", col, row);
        }
    }

    /// <summary>
    /// Static class containing a number of generators for lists of Hexes in various shapes.
    /// </summary>
    public static class ShapeGenerators
    {
        /// <summary>
        /// Enumeration for selecting which Axis to construct shapes along.
        /// </summary>
        public enum Axis
        {
            q = 0,
            r = 1,
            s = 2
        }

        /// <summary>
        /// Generates a parallelogram shaped collection of Hexes.
        /// </summary>
        /// <param name="axisX">The first axis to loop along.</param>
        /// <param name="axisY">The second axis to loop along.</param>
        /// <param name="x1">The start value on the first axis.</param>
        /// <param name="x2">The end value on the first axis.</param>
        /// <param name="y1">The start value on the second axis.</param>
        /// <param name="y2">The end value on the second axis.</param>
        /// <returns></returns>
        public static IEnumerable<Hex> Parallelogram(Axis axisX, Axis axisY, int x1, int x2, int y1, int y2)
        {
            var ax = (int)axisX;
            var ay = (int)axisY;

            for (int x = x1; x <= x2; x++)
            {
                for (int y = y1; y <= y2; y++)
                {
                    var h = new Hex();
                    h[ax] = x;
                    h[ay] = y;
                    h.SetThirdAxis(axisX, axisY);
                    yield return h;
                }
            }
        }

        /// <summary>
        /// Generates a triangle shaped collection of Hexes.
        /// </summary>
        /// <param name="size">Length of a side of the triangle.</param>
        /// <param name="invert">True to flip the direction the triangle points.</param>
        /// <returns></returns>
        public static IEnumerable<Hex> Triangle(int size, bool invert)
        {
            if (invert)
            {
                for (int q = 0; q <= size; q++)
                {
                    for (int r = 0; r <= size - q; r++)
                    {
                        yield return new Hex(q, r);
                    }
                }
            }
            else
            {
                for (int q = 0; q <= size; q++)
                {
                    for (int r = size - q; r <= size; r++)
                    {
                        yield return new Hex(q, r);
                    }
                }
            }
        }

        /// <summary>
        /// Generates a hexagon shaped collection of Hexes.
        /// </summary>
        /// <param name="radius">Radius of the generated Hexagon.</param>
        /// <returns></returns>
        public static IEnumerable<Hex> Hexagon(int radius)
        {
            for (int q = -radius; q <= radius; q++)
            {
                int r1 = Mathf.Max(-radius, -q - radius);
                int r2 = Mathf.Min(radius, -q + radius);
                for (int r = r1; r <= r2; r++)
                {
                    yield return new Hex(q, r, -q - r);
                }
            }
        }

        /// <summary>
        /// Generates a rectangle shaped collectoin of Hexes.
        /// </summary>
        /// <param name="axisX">The first axis to build the rectangle along.</param>
        /// <param name="axisY">The second axis to build the rectangle along.</param>
        /// <param name="height">The height of the rectangle.</param>
        /// <param name="width">The width of the rectangle.</param>
        /// <returns></returns>
        public static IEnumerable<Hex> Rectangle(Axis axisX, Axis axisY, int height, int width)
        {
            var ax = (int)axisX;
            var ay = (int)axisY;

            for (int y = 0; y < height; y++)
            {
                int y_offset = y >> 1;
                for (int x = -y_offset; x < width - y_offset; x++)
                {
                    var h = new Hex();
                    h[ax] = x;
                    h[ay] = y;
                    h.SetThirdAxis(axisX, axisY);
                    yield return h;
                }
            }
        }
    }
}